import React,{Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import ReactDOM from 'react-dom';
class NavBar extends Component
{
  state={
    name:null,
    movies:[]
  }
  textChange=(event)=>
  {
    this.setState({name:event.target.value})
  }
  formSubmit=(event)=>
  {
    axios.get('https://api.themoviedb.org/3/search/movie?query='+this.state.name+'&api_key=7b3f5966a0ec77fa7aa17152c7d27b15')
    .then(response => {
       //console.log(response.data.results);
      //this.setState(movies:)
      response.data.results.map(
        movie=>
        {
          this.setState({movies: [...this.state.movies, 
              {
                title:movie.title,
                average:movie.vote_average,
                overview:movie.overview,
                url:'https://image.tmdb.org/t/p/original'+movie.backdrop_path
              }
            ]
          })
        }
      )
      console.log(this.state)
    })
    .catch(error => {
      console.log(error);
    });
    event.preventDefault();
  }
render()
{
return(
  <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand font-weight-bold " id="title" href="#">MovieZilla</a>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" id="item" href="#">My Movies <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  <form class="form-inline " action="/action_page.php" onSubmit={this.formSubmit}>
    <input class="form-control mr-sm-2 " type="text" placeholder="Search" onChange={this.textChange} />
    <button class="btn btn-danger" type="submit">Search</button>
  </form>
</nav>
)
}

}
export default NavBar